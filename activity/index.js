console.log("Hello World"); 


//<-----------------------------------S15 ACTIVITY----------------------------------->


//#3 Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// - If the total of the two numbers is less than 10, add the numbers
// - If the total of the two numbers is 10 - 20, subtract the numbers
// - If the total of the two numbers is 21 - 29 multiply the numbers
// - If the total of the two numbers is greater than or equal to 30, divide the numbers


let firstNum = parseInt(prompt("Provide a number."));
let secondNum = parseInt(prompt("Provide another number"));
let totalOfTwo = firstNum + secondNum;


function compute(){
	if (totalOfTwo < 10){
		alert(firstNum + secondNum);
		console.log(firstNum + secondNum);
	} else if (totalOfTwo >= 10 && totalOfTwo <= 20){
		alert(firstNum - secondNum);
		console.log(firstNum - secondNum);
	} else if (totalOfTwo >= 21 && totalOfTwo <= 29){
		alert(firstNum * secondNum);
		console.log(firstNum * secondNum);
	} else if (totalOfTwo >= 30){
		alert(firstNum / secondNum);
		console.log(firstNum / secondNum);
	}
}
compute();


//#4 Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.

function alertWarn(){
	if (totalOfTwo >= 10){
		alert("The total of two number is equal or greater than 10.");
	} else if (totalOfTwo < 10){
		console.warn("The total of two number is less than 10.");
	}
}

alertWarn();


//#5 Prompt the user for their name and age and print out different alert messages based on the user input:
// - If the name OR age is blank/null, print the message are you a time traveler?
// - f the name AND age is not blank, print the message with the user’s name and age.

let userName = prompt("What is your name?");
let userAge = parseInt(prompt("What is your age?"));
let input = userName && userAge

function blankNull(){
		if (input.length == ''){
			alert("Are you a time traveler?");
		} else {
			alert(`Hello ${userName}! Your age is ${userAge}.`)
		}
	}

blankNull();


//#6 Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
// - 18 or greater, print an alert message saying You are of legal age.
// - 17 or less, print an alert message saying You are not allowed here.

function isLegalAge(){
	if (userAge >= 18) {
		alert("You are of legal age.");
	} else {
		alert("You are not allowed here!")
	}
}

isLegalAge()


//#7 Create a switch case statement that will check if the user's age input is within a certain set of expected input:
// - 18 - print the message You are now allowed to party.
// - 21 - print the message You are now part of the adult society.
// - 65 - print the message We thank you for your contribution to society.
// - Any other value - print the message Are you sure you're not an alien?

switch(userAge){
	case 18:
		alert("You are now allowed to party.")
		break;
	case 21:
		alert("You are now part of the adult society.")
		break;
	case 65:
		alert("We thank you for your contribution to society.")
		break;
	default:
		alert("Are you sure you're not an alien?");
		break;
	}	



//#8 Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.

function isLegalAge(){
	try {
		alert(isLeggalAge);
	}
	catch(err){
		console.log(typeof error);
		console.warn(err.message);
	}
	finally {
		console.log("Error!?")
	}
}